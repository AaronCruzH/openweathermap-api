import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { TestServiceService } from '../services/test-service.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  /*lista: String[]=['Hola','Msg','Adios'];

  json_list = [
    {
    title: "Hola",
    subtitle: "Msg",
    content:{ msg: "Description" }
    },
    {
      title: "Hola 2",
    subtitle: "Msg 2",
    content:{ msg: "Description 2" }
    }
  ]*/
  private weather: Observable<any>;
  constructor(private testService: TestServiceService) {
    
    this.weather = this.testService.getClima('');
    this.testService.getClima('').subscribe(res => console.log(res));
  }

}
