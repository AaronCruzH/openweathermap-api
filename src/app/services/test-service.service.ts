import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class TestServiceService {
  url: string = 'https://api.openweathermap.org/data/2.5/weather?q=Mexico&lang=es&appid=485940bcd87cfccb04241bea156b83c9'
  constructor(private http: HttpClient) {}

  getClima(city: string){
    return this.http.get<any>(this.url);
  } 
   
}
