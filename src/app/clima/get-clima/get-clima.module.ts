import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GetClimaPageRoutingModule } from './get-clima-routing.module';

import { GetClimaPage } from './get-clima.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GetClimaPageRoutingModule
  ],
  declarations: [GetClimaPage]
})
export class GetClimaPageModule {}
