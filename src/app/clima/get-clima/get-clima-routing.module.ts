import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GetClimaPage } from './get-clima.page';

const routes: Routes = [
  {
    path: '',
    component: GetClimaPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GetClimaPageRoutingModule {}
